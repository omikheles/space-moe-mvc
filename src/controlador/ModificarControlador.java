package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import modelo.InicioModelo;
import modelo.ModificarModelo;
import mvc.mvc;
import vista.InicioVista;
import vista.ModificarVista;

public class ModificarControlador extends mvc implements ActionListener {
	
	private ModificarVista view;	
	private ModificarModelo model;
	
	public ModificarControlador(ModificarVista view, ModificarModelo model) {
		this.view = view;
		this.model = model;		
		this.view.btnCrear.addActionListener(this);
	}
	
	public void iniciar(String nombreUsuario) {
		view.setTitle("MVC P5 - Modificar cuenta");
		view.setLocationRelativeTo(null);
		model.setNombreUsuario(nombreUsuario);
		model.getUser(nombreUsuario);
		
		view.txtNombreUsuario.setText(model.getNombreUsuario());
		view.txtEmail.setText(model.getTxtEmail());
		view.txtPassword.setText(model.getTxtPassword());
		view.txtTelefono.setText(model.getTxtTelefono());
			
		
	}
	
	@Override	
	public void actionPerformed(ActionEvent e) {
		
		model.setTxtNombreUsuario(view.txtNombreUsuario.getText());		
		model.setTxtPassword(view.txtPassword.getText());
		model.setTxtEmail(view.txtEmail.getText());
		model.setTxtTelefono(view.txtTelefono.getText());
		
		try {
			
			if(model.getTxtNombreUsuario().isEmpty() || model.getTxtPassword().isEmpty()) {
				view.lblMsg.setText("Nombre y contraseña no pueden ser vacios");
				throw new Exception("Nombre y contraseña no pueden ser vacios");				
			}
			
			if(model.modificarDatos()) {
				System.out.println("El usuario ha sido modificado");
				view.lblMsg.setForeground(Color.green);
				view.lblMsg.setText("Datos modificados con exito");
			} else {
				view.lblMsg.setText("El usuario ya existe o ha ocurrido un error. Intenta de nuevo.");	
			}
			
			
		} catch (Exception error) {
			// TODO Auto-generated catch block			
			error.printStackTrace();
		}
		
		
	}	

}

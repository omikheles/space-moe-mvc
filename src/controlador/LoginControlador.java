package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.LoginModelo;
import vista.LoginVista;

import mvc.mvc;

public class LoginControlador extends mvc implements ActionListener {	
	
	private LoginVista view;	
	private LoginModelo model;
	
	public LoginControlador(LoginVista view, LoginModelo model) {
		this.view = view;
		this.model = model;		
		this.view.btnEntrar.addActionListener(this);		
	}
	
	public void iniciar() {
		view.setTitle("MVC P5 - Iniciar sesion");
		view.setLocationRelativeTo(null);
	}
	
	@Override	
	public void actionPerformed(ActionEvent e) {
		
		model.setTxtUsuario(view.txtUsuario.getText());		
		model.setTxtPassword(view.txtPassword.getText());		
		
		if(model.login()) {	
			System.out.println("El Jugador ha entrado correctamente");
			System.out.println(model.getTxtUsuario());
			view.setVisible(false);
			datosUsuarioCtrl.iniciar(view.txtUsuario.getText());			
			datosUsuarioVista.setVisible(true);
		} else {			
			view.lblMsg.setText("Usuario no existe, comprueba el login y contraseña");
			System.out.println("Usuario no existe, comprueba el login y contraseña");
		}
	}	

}

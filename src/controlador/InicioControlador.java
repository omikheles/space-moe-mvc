package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.InicioModelo;
import vista.InicioVista;

public class InicioControlador implements ActionListener {
	
	private InicioVista view;	
	private InicioModelo model;
	
	public InicioControlador(InicioVista view, InicioModelo model) {
		this.view = view;
		this.model = model;			
	}
	
	public void iniciar() {
		view.setTitle("MVC P5 - Inicio");
		view.setLocationRelativeTo(null);
	}
	
	@Override	
	public void actionPerformed(ActionEvent e) {
		
	}	

}

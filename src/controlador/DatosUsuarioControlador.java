package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.DatosUsuarioModelo;
import mvc.mvc;
import vista.DatosUsuarioVista;

public class DatosUsuarioControlador extends mvc implements ActionListener {
	
	private DatosUsuarioVista view;	
	private DatosUsuarioModelo model;
	
	public DatosUsuarioControlador(DatosUsuarioVista view, DatosUsuarioModelo model) {
		this.view = view;
		this.model = model;	
		this.view.btnModificar.addActionListener(this);			
	}
	
	public void iniciar(String nombreUsuario) {
		view.setTitle("MVC P5 - Datos usuario");
		view.setLocationRelativeTo(null);
		view.lblTitle.setText("Bienvenido "+nombreUsuario);
		view.lblPuntos.setText(String.valueOf(model.getPuntuacion(nombreUsuario)));
		model.setNombreUsuario(nombreUsuario);		
	}
	
	@Override	
	public void actionPerformed(ActionEvent e) {
		datosUsuarioVista.setVisible(false);				
		modificarCtrl.iniciar(model.getNombreUsuario());
		modificarVista.setVisible(true);		
	}	

}

package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import com.mysql.cj.util.StringUtils;

import modelo.InicioModelo;
import modelo.RegistroModelo;
import mvc.mvc;
import vista.InicioVista;
import vista.RegistroVista;

public class RegistroControlador extends mvc implements ActionListener {
	
	private RegistroVista view;	
	private RegistroModelo model;
	
	public RegistroControlador(RegistroVista view, RegistroModelo model) {
		this.view = view;
		this.model = model;		
		this.view.btnCrear.addActionListener(this);
	}
	
	public void iniciar() {
		view.setTitle("MVC P5 - Crear cuenta");
		view.setLocationRelativeTo(null);
	}
	
	@Override	
	public void actionPerformed(ActionEvent e) {
		
		model.setTxtNombreUsuario(view.txtNombreUsuario.getText());		
		model.setTxtPassword(view.txtPassword.getText());
		model.setTxtEmail(view.txtEmail.getText());
		model.setTxtTelefono(view.txtTelefono.getText());
				
		try {
			
			if(model.getTxtNombreUsuario().isEmpty() || model.getTxtPassword().isEmpty()) {
				view.lblMsg.setText("Nombre y contraseña no pueden ser vacios");
				throw new Exception("Nombre y contraseña no pueden ser vacios");				
			}
			
			if(model.registrar()) {	
				System.out.println("El usuario ha sido registrado con nombre: "+model.getTxtNombreUsuario()+" y contraseña:"+model.getTxtPassword());
				view.setVisible(false);
				datosUsuarioCtrl.iniciar(view.txtNombreUsuario.getText());
				datosUsuarioVista.setVisible(true);
			} else {
				view.lblMsg.setText("El usuario ya existe o ha ocurrido un error. Intenta de nuevo.");				
			}
		
		} catch (Exception error) {
			// TODO Auto-generated catch block			
			error.printStackTrace();
		}
		
		
	}	

}

package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.Font;
import javax.swing.JButton;

public class InicioVista extends JFrame {

	private JPanel contentPane;
	public JButton btnLogin;
	public JButton btnCrearCuenta;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InicioVista frame = new InicioVista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InicioVista() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel logo = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/spacemoe.png")).getImage();
		contentPane.setLayout(null);
		logo.setIcon(new ImageIcon(img));
		logo.setBounds(118, 60, 297, 109);
		contentPane.add(logo);		
		
		JLabel lblGrupo = new JLabel("Oleg, Maria, Elizabeth - Grupo M.O.E, 2019");
		lblGrupo.setFont(new Font("Arial", Font.PLAIN, 11));
		lblGrupo.setBounds(170, 346, 204, 14);
		contentPane.add(lblGrupo);
		
		btnLogin = new JButton("ENTRAR");
		btnLogin.setBounds(118, 206, 132, 52);
		contentPane.add(btnLogin);
		
		btnCrearCuenta = new JButton("CREAR CUENTA");
		btnCrearCuenta.setBounds(283, 206, 132, 52);
		contentPane.add(btnCrearCuenta);
	}
}

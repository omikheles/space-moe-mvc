package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

public class DatosUsuarioVista extends JFrame {

	private JPanel contentPane;
	public JButton btnJugar;
	public JButton btnModificar;
	public JLabel lblPuntos;
	public JLabel lblTitle;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DatosUsuarioVista frame = new DatosUsuarioVista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DatosUsuarioVista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel logo = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/spacemoe-small.png")).getImage();
		logo.setIcon(new ImageIcon(img));
		logo.setBounds(173, 18, 187, 68);
		contentPane.add(logo);
		
		lblTitle = new JLabel("Bienvenido #NombreUsuario#");
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setFont(new Font("Arial", Font.PLAIN, 18));
		lblTitle.setBounds(64, 115, 405, 14);
		contentPane.add(lblTitle);
		
		JLabel label = new JLabel("Oleg, Maria, Elizabeth - Grupo M.O.E, 2019");
		label.setFont(new Font("Arial", Font.PLAIN, 11));
		label.setBounds(165, 336, 204, 14);
		contentPane.add(label);
		
		JLabel lblTexto = new JLabel("Su puntuaci\u00F3n m\u00E1xima es de:");
		lblTexto.setHorizontalAlignment(SwingConstants.CENTER);
		lblTexto.setFont(new Font("Arial", Font.PLAIN, 12));
		lblTexto.setBounds(139, 155, 255, 14);
		contentPane.add(lblTexto);
		
		btnJugar = new JButton("JUGAR");
		btnJugar.setFont(new Font("Arial", Font.PLAIN, 11));
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnJugar.setBounds(126, 242, 130, 50);
		contentPane.add(btnJugar);
		
		btnModificar = new JButton("MODIFICAR DATOS");
		btnModificar.setFont(new Font("Arial", Font.PLAIN, 11));
		btnModificar.setBounds(272, 242, 130, 50);
		contentPane.add(btnModificar);
		
		lblPuntos = new JLabel("35.520");
		lblPuntos.setFont(new Font("Tahoma", Font.BOLD, 23));
		lblPuntos.setHorizontalAlignment(SwingConstants.CENTER);
		lblPuntos.setBounds(196, 180, 142, 38);
		contentPane.add(lblPuntos);
		
	}
}

package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Color;

public class LoginVista extends JFrame {

	private JPanel contentPane;
	public JTextField txtUsuario;
	public JPasswordField txtPassword;
	public JButton btnEntrar;
	public JButton btnInicio;
	public JLabel lblMsg;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginVista frame = new LoginVista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginVista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel logo = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/spacemoe-small.png")).getImage();
		logo.setIcon(new ImageIcon(img));
		logo.setBounds(173, 18, 187, 68);
		contentPane.add(logo);
		
		JLabel lblTitle = new JLabel("Iniciar sesi\u00F3n");
		lblTitle.setFont(new Font("Arial", Font.PLAIN, 18));
		lblTitle.setBounds(215, 115, 104, 14);
		contentPane.add(lblTitle);
		
		JLabel label = new JLabel("Oleg, Maria, Elizabeth - Grupo M.O.E, 2019");
		label.setFont(new Font("Arial", Font.PLAIN, 11));
		label.setBounds(165, 336, 204, 14);
		contentPane.add(label);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Arial", Font.BOLD, 12));
		lblUsuario.setBounds(161, 171, 44, 14);
		contentPane.add(lblUsuario);
		
		txtUsuario = new JTextField();
		txtUsuario.setFont(new Font("Arial", Font.PLAIN, 12));
		txtUsuario.setBounds(220, 163, 170, 30);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel lblPassword = new JLabel("Contrase\u00F1a");
		lblPassword.setFont(new Font("Arial", Font.BOLD, 12));
		lblPassword.setBounds(139, 215, 66, 14);
		contentPane.add(lblPassword);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(220, 207, 170, 30);
		contentPane.add(txtPassword);
		
		btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(290, 251, 100, 30);
		contentPane.add(btnEntrar);
		
		btnInicio = new JButton("Inicio");
		Image imgInicio = new ImageIcon(this.getClass().getResource("/home.png")).getImage();
		btnInicio.setIcon(new ImageIcon(imgInicio));
		btnInicio.setBounds(10, 11, 89, 23);
		contentPane.add(btnInicio);
		
		lblMsg = new JLabel("");
		lblMsg.setForeground(Color.RED);
		lblMsg.setHorizontalAlignment(SwingConstants.CENTER);
		lblMsg.setFont(new Font("Arial", Font.PLAIN, 11));
		lblMsg.setBounds(48, 298, 438, 14);
		contentPane.add(lblMsg);
		
	}
}

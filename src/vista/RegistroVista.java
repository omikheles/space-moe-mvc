package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.SwingConstants;

public class RegistroVista extends JFrame {

	private JPanel contentPane;
	public JTextField txtNombreUsuario;
	public JPasswordField txtPassword;
	public JButton btnCrear;
	public JTextField txtEmail;
	public JTextField txtTelefono;
	public JButton btnInicio;
	public JLabel lblMsg;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistroVista frame = new RegistroVista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistroVista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel logo = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/spacemoe-small.png")).getImage();
		logo.setIcon(new ImageIcon(img));
		logo.setBounds(173, 18, 187, 68);
		contentPane.add(logo);
		
		JLabel lblTitle = new JLabel("Crear cuenta");
		lblTitle.setFont(new Font("Arial", Font.PLAIN, 18));
		lblTitle.setBounds(215, 115, 104, 14);
		contentPane.add(lblTitle);
		
		JLabel label = new JLabel("Oleg, Maria, Elizabeth - Grupo M.O.E, 2019");
		label.setFont(new Font("Arial", Font.PLAIN, 11));
		label.setBounds(165, 336, 204, 14);
		contentPane.add(label);
		
		JLabel lblNombreUsuario = new JLabel("Nombre usuario");
		lblNombreUsuario.setFont(new Font("Arial", Font.BOLD, 12));
		lblNombreUsuario.setBounds(87, 156, 91, 14);
		contentPane.add(lblNombreUsuario);
		
		txtNombreUsuario = new JTextField();
		txtNombreUsuario.setFont(new Font("Arial", Font.PLAIN, 12));
		txtNombreUsuario.setBounds(87, 176, 170, 30);
		contentPane.add(txtNombreUsuario);
		txtNombreUsuario.setColumns(10);
		
		JLabel lblPassword = new JLabel("Contrase\u00F1a");
		lblPassword.setFont(new Font("Arial", Font.BOLD, 12));
		lblPassword.setBounds(288, 156, 66, 14);
		contentPane.add(lblPassword);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(288, 175, 170, 30);
		contentPane.add(txtPassword);
		
		btnCrear = new JButton("Crear cuenta");
		btnCrear.setBounds(87, 282, 371, 30);
		contentPane.add(btnCrear);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setFont(new Font("Arial", Font.BOLD, 12));
		lblEmail.setBounds(87, 217, 91, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setFont(new Font("Arial", Font.PLAIN, 12));
		txtEmail.setColumns(10);
		txtEmail.setBounds(87, 237, 170, 30);
		contentPane.add(txtEmail);
		
		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setFont(new Font("Arial", Font.BOLD, 12));
		lblTelefono.setBounds(288, 216, 91, 14);
		contentPane.add(lblTelefono);
		
		txtTelefono = new JTextField();
		txtTelefono.setFont(new Font("Arial", Font.PLAIN, 12));
		txtTelefono.setColumns(10);
		txtTelefono.setBounds(288, 236, 170, 30);
		contentPane.add(txtTelefono);
		
		btnInicio = new JButton("Inicio");
		Image imgInicio = new ImageIcon(this.getClass().getResource("/home.png")).getImage();
		btnInicio.setIcon(new ImageIcon(imgInicio));
		btnInicio.setBounds(10, 11, 89, 23);
		contentPane.add(btnInicio);
		
		lblMsg = new JLabel("");
		lblMsg.setHorizontalAlignment(SwingConstants.CENTER);
		lblMsg.setForeground(Color.RED);
		lblMsg.setFont(new Font("Arial", Font.PLAIN, 11));
		lblMsg.setBounds(48, 318, 438, 14);
		contentPane.add(lblMsg);
		
	}
}

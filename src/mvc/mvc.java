package mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controlador.DatosUsuarioControlador;
import controlador.InicioControlador;
import controlador.LoginControlador;
import controlador.ModificarControlador;
import controlador.RegistroControlador;
import modelo.DatosUsuarioModelo;
import modelo.InicioModelo;
import modelo.LoginModelo;
import modelo.ModificarModelo;
import modelo.RegistroModelo;
import vista.DatosUsuarioVista;
import vista.InicioVista;
import vista.LoginVista;
import vista.ModificarVista;
import vista.RegistroVista;

public class mvc {	
	

	protected static DatosUsuarioVista datosUsuarioVista;
	protected static DatosUsuarioControlador datosUsuarioCtrl;
	
	protected static RegistroVista registroVista;
	protected static RegistroControlador registroCtrl;
	
	protected static ModificarVista modificarVista;
	protected static ModificarControlador modificarCtrl;

	public static void main(String[] args) {	
		
		//Inicio 
		InicioModelo inicioModelo = new InicioModelo();
		InicioVista inicioVista = new InicioVista(); 
		InicioControlador inicioCtrl = new InicioControlador(inicioVista,inicioModelo);
		
		// Login
		LoginModelo loginModel = new LoginModelo();
		LoginVista loginVista = new LoginVista();		
		LoginControlador loginCtrl = new LoginControlador(loginVista,loginModel);
				
		// Datos usuario
		DatosUsuarioModelo datosUsuarioModel = new DatosUsuarioModelo();
		datosUsuarioVista = new DatosUsuarioVista();		
		datosUsuarioCtrl = new DatosUsuarioControlador(datosUsuarioVista,datosUsuarioModel);
		
		// Registro
		RegistroModelo registroModel = new RegistroModelo();
		registroVista = new RegistroVista();		
		registroCtrl = new RegistroControlador(registroVista,registroModel);
		
		// Modificar
		ModificarModelo modificarModel = new ModificarModelo();
		modificarVista = new ModificarVista();		
		modificarCtrl = new ModificarControlador(modificarVista,modificarModel);
		
		//ctrl.iniciar();
		//view.setVisible(true);		
		
		// Iniciamos nuestra aplicación con la primera vista
		inicioCtrl.iniciar();
		inicioVista.setVisible(true);
		
		// --------------------------------------------------------------------
		// ENRUTADOR DE NUESTRA APLICACIÓN BASADO EN LOS EVENTOS DE BOTONES
		// --------------------------------------------------------------------
		
		// Abrimos la vista de login
		inicioVista.btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {								
				inicioVista.setVisible(false);				
				loginCtrl.iniciar();
				loginVista.setVisible(true);
			}
		});	
		
		inicioVista.btnCrearCuenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {								
				inicioVista.setVisible(false);				
				registroCtrl.iniciar();
				registroVista.setVisible(true);
			}
		});			
		
//		datosUsuarioVista.btnModificar.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {								
//				datosUsuarioVista.setVisible(false);				
//				modificarCtrl.iniciar();
//				modificarVista.setVisible(true);
//			}
//		});	
		
		
		// Volver a inicio		
		registroVista.btnInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {								
				registroVista.setVisible(false);				
				inicioCtrl.iniciar();
				inicioVista.setVisible(true);
			}
		});
		
		modificarVista.btnInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {								
				modificarVista.setVisible(false);				
				inicioCtrl.iniciar();
				inicioVista.setVisible(true);
			}
		});
		
		loginVista.btnInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {								
				loginVista.setVisible(false);				
				inicioCtrl.iniciar();
				inicioVista.setVisible(true);
			}
		});
		
		
	}

}


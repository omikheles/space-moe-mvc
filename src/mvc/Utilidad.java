package mvc;

import java.sql.*;

public class Utilidad {
	
	private static String host = "jdbc:mysql://localhost:3306/proyecto6?useTimezone=true&serverTimezone=UTC";
	private static String driver = "com.mysql.cj.jdbc.Driver";	
	private static String username = "root";
	private static String password = "";
	private static Connection con;
	
	/**
	 * Constructor de Utilidad
	 * @return con Connection
	 */	
	public Connection conectar() {
		
		try{
			
			Class.forName(driver);						
			con = DriverManager.getConnection(host,username,password);						  		
		
		} catch(Exception e){
			
			System.out.println(e);
			
		}
		
		return con;
	}		
	

}

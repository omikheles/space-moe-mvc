package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import mvc.Utilidad;

public class ModificarModelo {

	private static Utilidad utilidad = new Utilidad();
	private static Connection con = utilidad.conectar();
	
	private String txtNombreUsuario;
	private String txtPassword;
	private String txtEmail;
	private String txtTelefono;
	private String nombreUsuario;
	
	public String getTxtNombreUsuario() {
		return txtNombreUsuario;
	}
	public void setTxtNombreUsuario(String txtNombreUsuario) {
		this.txtNombreUsuario = txtNombreUsuario;
	}
	public String getTxtPassword() {
		return txtPassword;
	}
	public void setTxtPassword(String txtPassword) {
		this.txtPassword = txtPassword;
	}
	public String getTxtEmail() {
		return txtEmail;
	}
	public void setTxtEmail(String txtEmail) {
		this.txtEmail = txtEmail;
	}
	public String getTxtTelefono() {
		return txtTelefono;
	}
	public void setTxtTelefono(String txtTelefono) {
		this.txtTelefono = txtTelefono;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
//	public ModificarModelo() {
//		// TODO Persistencia de la aplicación con mapeo OR
//	}
	
	/**
	 * Recuperamos datos de usuario
	 * @param nombreUsuario El nombre de usuario
	 */
	public void getUser(String nombreUsuario){
		ResultSet rs = null;
		try {				
			
			PreparedStatement statement = con.prepareStatement("SELECT * FROM usuarios WHERE nombre = ? LIMIT 1");
			statement.setString(1, nombreUsuario);
			rs = statement.executeQuery();	
			if(rs.next()) {
				setNombreUsuario(rs.getString("nombre"));
				setTxtTelefono(rs.getString("telefono"));
				setTxtPassword(rs.getString("password"));
				setTxtEmail(rs.getString("email"));				
			}			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
						
	}
	
	/** Editar datos de usuario
	 * @return True or false
	 */
	public boolean modificarDatos() {
		
		try {
			
			if(this.checkUsuarioExiste(txtNombreUsuario)) {
				throw new SQLException("REGISTRO: Usuario existe");
			}
			
			PreparedStatement statement = con.prepareStatement("UPDATE usuarios SET nombre = ?, password = ?, email = ?, telefono = ? WHERE nombre = ?");
			statement.setString(1, txtNombreUsuario);
			statement.setString(2, txtPassword);
			statement.setString(3, txtEmail);
			statement.setString(4, txtTelefono);
			statement.setString(5, nombreUsuario);
			
			statement.executeUpdate();			
								
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/**
	 * Comprobamos si el usuario existe en la base
	 * @param email El e-mail de usuario
	 * @return true o false
	 */
	public boolean checkUsuarioExiste(String nombre) {
		
			
		try {		
			
			PreparedStatement statement = con.prepareStatement("SELECT email FROM usuarios WHERE nombre = ? LIMIT 1");
			statement.setString(1, nombre);
			ResultSet rs = statement.executeQuery();
			
			if(!rs.next()) {
				return false;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
		return true;
				
	}

}

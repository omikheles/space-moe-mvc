package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mvc.Utilidad;

public class RegistroModelo {
	
	private static Utilidad utilidad = new Utilidad();
	private static Connection con = utilidad.conectar();
	
	private String txtNombreUsuario;
	private String txtPassword;
	private String txtEmail;
	private String txtTelefono;	

	public String getTxtNombreUsuario() {
		return txtNombreUsuario;
	}

	public void setTxtNombreUsuario(String txtNombreUsuario) {
		this.txtNombreUsuario = txtNombreUsuario;
	}

	public String getTxtPassword() {
		return txtPassword;
	}

	public void setTxtPassword(String txtPassword) {
		this.txtPassword = txtPassword;
	}

	public String getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(String txtEmail) {
		this.txtEmail = txtEmail;
	}

	public String getTxtTelefono() {
		return txtTelefono;
	}

	public void setTxtTelefono(String txtTelefono) {
		this.txtTelefono = txtTelefono;
	}

//	public RegistroModelo() {
//		// TODO Persistencia de la aplicación con mapeo OR
//	}
	
	/** Registramos el usuario 	 
	 * @return True o False
	 * @throws SQLException SQL Errors
	 */		
	public boolean registrar() throws SQLException {							
						
		try (					
			PreparedStatement statement = con.prepareStatement("INSERT INTO  puntuaciones(puntuacion) VALUES (?)", Statement.RETURN_GENERATED_KEYS);			
			PreparedStatement statement2 = con.prepareStatement("INSERT INTO  Usuarios (nombre, password, email, telefono, nivelid,puntuacionid) VALUES (?, ?, ?, ?, ?, ?);	", Statement.RETURN_GENERATED_KEYS);
		)
		{
			
			con.setAutoCommit(false);					
			
			if(this.checkUsuarioExiste(txtNombreUsuario)) {
				throw new SQLException("REGISTRO: Usuario existe");
			}
			
			statement.setInt(1, 0);
			statement.executeUpdate();							
			
			try(ResultSet generatedKeys = statement.getGeneratedKeys()){
				if (generatedKeys.next()) {
					System.out.println("Id puntuacion: " + generatedKeys.getLong(1));	
					statement2.setString(1, txtNombreUsuario);
					statement2.setString(2, txtPassword);
					statement2.setString(3, txtEmail);
					statement2.setString(4, txtTelefono);
					statement2.setInt(5, 1);
					statement2.setLong(6, generatedKeys.getLong(1));
					statement2.executeUpdate();					
				} else {
					throw new SQLException("Failed");
				}
			}
			
			con.commit();														
			
		} catch (SQLException e) {
			
			con.rollback();			
			System.out.println(e.getMessage());
			
			return false;
		}
		
		return true;
		
	}
	
	/**
	 * Comprobamos si el usuario existe en la base
	 * @param email El e-mail de usuario
	 * @return true o false
	 */
	public boolean checkUsuarioExiste(String nombre) {
		
			
		try {		
			
			PreparedStatement statement = con.prepareStatement("SELECT email FROM usuarios WHERE nombre = ? LIMIT 1");
			statement.setString(1, nombre);
			ResultSet rs = statement.executeQuery();
			
			if(!rs.next()) {
				return false;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
		return true;
				
	}

}

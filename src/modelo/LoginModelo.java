package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JPasswordField;
import javax.swing.JTextField;

import mvc.Utilidad;

public class LoginModelo {
	
	private static Utilidad utilidad = new Utilidad();
	private static Connection con = utilidad.conectar();
	
	private String txtUsuario;
	private String txtPassword;

	public String getTxtUsuario() {
		return txtUsuario;
	}

	public void setTxtUsuario(String txtUsuario) {
		this.txtUsuario = txtUsuario;
	}

	public String getTxtPassword() {
		return txtPassword;
	}

	public void setTxtPassword(String txtPassword) {
		this.txtPassword = txtPassword;
	}
	
	/** Login de usuario
	 * @return True or false
	 */
	public boolean login() {
					
		try {			
			
			PreparedStatement statement = con.prepareStatement("SELECT IdUsuario,email FROM usuarios WHERE nombre = ? AND password = ? LIMIT 1");
			statement.setString(1, txtUsuario);
			statement.setString(2, txtPassword);
			
			ResultSet rs = statement.executeQuery();
			
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
			// Captamos cualquier error.	
			e.printStackTrace();
		}
		
		return false;
		
	}
	
//	public LoginModelo() {
//	// TODO Persistencia de la aplicación con mapeo OR
//}

}

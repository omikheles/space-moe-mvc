package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JLabel;

import mvc.Utilidad;

public class DatosUsuarioModelo {
	
	private static Utilidad utilidad = new Utilidad();
	private static Connection con = utilidad.conectar();

	private String lblPuntos;
	private String lblTitle;
	private String nombreUsuario;
	
	public String getLblPuntos() {
		return lblPuntos;
	}
	public void setLblPuntos(String lblPuntos) {
		this.lblPuntos = lblPuntos;
	}
	public String getLblTitle() {
		return lblTitle;
	}
	public void setLblTitle(String lblTitle) {
		this.lblTitle = lblTitle;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
	/**
	 * Recuperamos el ID de puntuacion
	 * @param email El e-mail de usuario
	 * @return El id de puntuacion
	 */
	public int getIdPuntuacion(String nombreUsuario){
		int id = 0;
		try {				
			
			PreparedStatement statement = con.prepareStatement("SELECT puntuacionid FROM usuarios WHERE nombre = ? LIMIT 1");
			statement.setString(1, nombreUsuario);
			ResultSet rs = statement.executeQuery();
			
			if(rs.next()) {					
				id = rs.getInt("puntuacionid");			
			}					
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return id;
	}
	
	/**
	 * Recuperamos puntuacion de Jugador
	 * @param email El e-mail de usuario
	 * @return puntos Los puntos de usuario
	 */
	public int getPuntuacion(String nombreUsuario){
		int puntos = 0;
		try {				
			
			PreparedStatement statement = con.prepareStatement("SELECT puntuacion FROM puntuaciones WHERE IdPuntuacion = ? LIMIT 1");
			statement.setInt(1, this.getIdPuntuacion(nombreUsuario));
			ResultSet rs = statement.executeQuery();
			
			if(rs.next()) {					
				puntos = rs.getInt("puntuacion");			
			}					
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return puntos;
	}

}
